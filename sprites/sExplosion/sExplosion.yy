{
    "id": "ddde8b8e-b96a-47ad-aeb0-c36d71587c30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sExplosion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 3,
    "bbox_right": 43,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "590f2dbc-2746-47b4-9d1a-c0f783a9f713",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddde8b8e-b96a-47ad-aeb0-c36d71587c30",
            "compositeImage": {
                "id": "a9519558-78b2-437c-a5d5-25c3c082f83a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "590f2dbc-2746-47b4-9d1a-c0f783a9f713",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28786bbf-93b2-47cd-a1d1-2c9080eb2947",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "590f2dbc-2746-47b4-9d1a-c0f783a9f713",
                    "LayerId": "503bee00-8a1c-43cd-93a1-e0df1cf9f31f"
                }
            ]
        },
        {
            "id": "7ec595fc-78d2-485c-a40f-6b645e83450c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddde8b8e-b96a-47ad-aeb0-c36d71587c30",
            "compositeImage": {
                "id": "b8fc323e-13d1-4630-b495-94df5364f0fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ec595fc-78d2-485c-a40f-6b645e83450c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "758af91f-619e-4b91-93cc-05328138973a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ec595fc-78d2-485c-a40f-6b645e83450c",
                    "LayerId": "503bee00-8a1c-43cd-93a1-e0df1cf9f31f"
                }
            ]
        },
        {
            "id": "e3e84345-69cd-4f2b-8f00-582cf061792f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddde8b8e-b96a-47ad-aeb0-c36d71587c30",
            "compositeImage": {
                "id": "49767fba-f233-4947-8b06-f8a483a2b6dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3e84345-69cd-4f2b-8f00-582cf061792f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "154e361d-23fe-4003-b965-166ed90c0a64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3e84345-69cd-4f2b-8f00-582cf061792f",
                    "LayerId": "503bee00-8a1c-43cd-93a1-e0df1cf9f31f"
                }
            ]
        },
        {
            "id": "f15aeca6-aaa2-4dee-94af-1ebcbbd0af7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddde8b8e-b96a-47ad-aeb0-c36d71587c30",
            "compositeImage": {
                "id": "99043582-3c18-41c8-be30-e8e6c5fbc357",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f15aeca6-aaa2-4dee-94af-1ebcbbd0af7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2448b6ac-914f-42e6-82da-fccc3527edc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f15aeca6-aaa2-4dee-94af-1ebcbbd0af7c",
                    "LayerId": "503bee00-8a1c-43cd-93a1-e0df1cf9f31f"
                }
            ]
        },
        {
            "id": "09083096-5b96-4e83-adfd-8f0ef09c5cf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddde8b8e-b96a-47ad-aeb0-c36d71587c30",
            "compositeImage": {
                "id": "e858a407-a969-4554-af3a-d5d5d39d69d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09083096-5b96-4e83-adfd-8f0ef09c5cf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2000d028-e368-489f-b026-94dbf91f17f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09083096-5b96-4e83-adfd-8f0ef09c5cf0",
                    "LayerId": "503bee00-8a1c-43cd-93a1-e0df1cf9f31f"
                }
            ]
        },
        {
            "id": "085fd228-a5c5-4a18-bad5-f998f6a10d0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddde8b8e-b96a-47ad-aeb0-c36d71587c30",
            "compositeImage": {
                "id": "204b39e0-e5c1-4330-a7b1-3995dbbc2138",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "085fd228-a5c5-4a18-bad5-f998f6a10d0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c763dde6-9b4d-4c76-828a-0ae70f223514",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "085fd228-a5c5-4a18-bad5-f998f6a10d0b",
                    "LayerId": "503bee00-8a1c-43cd-93a1-e0df1cf9f31f"
                }
            ]
        },
        {
            "id": "b121ef48-5b8d-4eae-8727-506f2012e6aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddde8b8e-b96a-47ad-aeb0-c36d71587c30",
            "compositeImage": {
                "id": "5db3f23b-50e6-4974-955f-f8269ac971dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b121ef48-5b8d-4eae-8727-506f2012e6aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b61036ca-db66-4a5b-a1da-e6f35d1ebac8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b121ef48-5b8d-4eae-8727-506f2012e6aa",
                    "LayerId": "503bee00-8a1c-43cd-93a1-e0df1cf9f31f"
                }
            ]
        },
        {
            "id": "d2ec60d3-ac60-48d1-a487-a8bd349bea1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddde8b8e-b96a-47ad-aeb0-c36d71587c30",
            "compositeImage": {
                "id": "5778c0d8-c732-47b5-b419-d1711d1355ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2ec60d3-ac60-48d1-a487-a8bd349bea1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a761499-4216-4e69-bf08-a8b9e236cf61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2ec60d3-ac60-48d1-a487-a8bd349bea1f",
                    "LayerId": "503bee00-8a1c-43cd-93a1-e0df1cf9f31f"
                }
            ]
        },
        {
            "id": "515ff1eb-fe9e-4de3-b09c-15f0ee5d6981",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddde8b8e-b96a-47ad-aeb0-c36d71587c30",
            "compositeImage": {
                "id": "31b77f19-a408-422c-a6c2-741044f6f302",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "515ff1eb-fe9e-4de3-b09c-15f0ee5d6981",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4bba66c-8f3a-437c-a302-1373a5e0497e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "515ff1eb-fe9e-4de3-b09c-15f0ee5d6981",
                    "LayerId": "503bee00-8a1c-43cd-93a1-e0df1cf9f31f"
                }
            ]
        },
        {
            "id": "795ee461-b4ee-4aa0-808e-a62b1c431e0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddde8b8e-b96a-47ad-aeb0-c36d71587c30",
            "compositeImage": {
                "id": "86e49620-1711-4291-9af8-b3736e4d2a56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "795ee461-b4ee-4aa0-808e-a62b1c431e0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5101184-3704-4b23-84b7-1630d51510a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "795ee461-b4ee-4aa0-808e-a62b1c431e0b",
                    "LayerId": "503bee00-8a1c-43cd-93a1-e0df1cf9f31f"
                }
            ]
        },
        {
            "id": "1e07ec1d-7859-40f6-82f2-ff28872d9202",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddde8b8e-b96a-47ad-aeb0-c36d71587c30",
            "compositeImage": {
                "id": "3d721663-529f-42d5-8519-da95cc6a6038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e07ec1d-7859-40f6-82f2-ff28872d9202",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfd856d4-f4ce-49ff-9128-286951c91fdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e07ec1d-7859-40f6-82f2-ff28872d9202",
                    "LayerId": "503bee00-8a1c-43cd-93a1-e0df1cf9f31f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "503bee00-8a1c-43cd-93a1-e0df1cf9f31f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ddde8b8e-b96a-47ad-aeb0-c36d71587c30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 23
}