{
    "id": "954af1b6-0a01-4880-8211-f36b7cb75197",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFieldOfView",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 329,
    "bbox_left": 0,
    "bbox_right": 349,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca87ed77-c013-4850-80f4-0d5bf0fe6769",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "954af1b6-0a01-4880-8211-f36b7cb75197",
            "compositeImage": {
                "id": "1f988354-af95-4b75-866e-3eeda5aca785",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca87ed77-c013-4850-80f4-0d5bf0fe6769",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8076c541-89cb-4b1a-9020-cc468ccaa0e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca87ed77-c013-4850-80f4-0d5bf0fe6769",
                    "LayerId": "63134cba-c8a5-4732-80c1-bdbf38a298c5"
                }
            ]
        },
        {
            "id": "99644aca-67b3-45d4-9778-fe8454bb413e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "954af1b6-0a01-4880-8211-f36b7cb75197",
            "compositeImage": {
                "id": "34503731-1d1a-40d2-be0c-9a6c74714562",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99644aca-67b3-45d4-9778-fe8454bb413e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "449f374a-39fe-4911-b18f-d26cebaf8930",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99644aca-67b3-45d4-9778-fe8454bb413e",
                    "LayerId": "63134cba-c8a5-4732-80c1-bdbf38a298c5"
                }
            ]
        },
        {
            "id": "dabbd84b-6a36-44a4-b885-30585a4c9e5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "954af1b6-0a01-4880-8211-f36b7cb75197",
            "compositeImage": {
                "id": "b3402da5-600c-4f34-af6a-4f2017f84d5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dabbd84b-6a36-44a4-b885-30585a4c9e5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfa8660a-6873-4643-a639-7dc5d1257954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dabbd84b-6a36-44a4-b885-30585a4c9e5d",
                    "LayerId": "63134cba-c8a5-4732-80c1-bdbf38a298c5"
                }
            ]
        },
        {
            "id": "43688ea8-1947-4b4e-9d86-3c9a203eb85d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "954af1b6-0a01-4880-8211-f36b7cb75197",
            "compositeImage": {
                "id": "b9a4ad6f-2596-4dd2-be85-1980f4669a8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43688ea8-1947-4b4e-9d86-3c9a203eb85d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7570e8fb-93d1-4640-b6cb-f106a0d37eae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43688ea8-1947-4b4e-9d86-3c9a203eb85d",
                    "LayerId": "63134cba-c8a5-4732-80c1-bdbf38a298c5"
                }
            ]
        }
    ],
    "gridX": 5,
    "gridY": 5,
    "height": 350,
    "layers": [
        {
            "id": "63134cba-c8a5-4732-80c1-bdbf38a298c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "954af1b6-0a01-4880-8211-f36b7cb75197",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 350,
    "xorig": -20,
    "yorig": 175
}