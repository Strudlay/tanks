{
    "id": "954c8464-aa56-4d74-af4a-8b44dcf69405",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tankbase_03",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 18,
    "bbox_right": 77,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f9321d99-ae91-4da1-8a97-e57724f8f0b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "954c8464-aa56-4d74-af4a-8b44dcf69405",
            "compositeImage": {
                "id": "81ed232c-7ce8-46e8-b1de-d8c130208311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9321d99-ae91-4da1-8a97-e57724f8f0b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ecc6d8d-84f5-4b2b-91cc-955ffd2f05d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9321d99-ae91-4da1-8a97-e57724f8f0b1",
                    "LayerId": "7cdaa765-3b1c-48c2-af03-2c8198fb641a"
                }
            ]
        },
        {
            "id": "bdb31f94-0ff4-4afb-a421-af791de66b89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "954c8464-aa56-4d74-af4a-8b44dcf69405",
            "compositeImage": {
                "id": "890885a4-a3d7-41cb-b98e-0ff4a212ecc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdb31f94-0ff4-4afb-a421-af791de66b89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4446c21-11b4-4114-8cf2-c65a3a959120",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdb31f94-0ff4-4afb-a421-af791de66b89",
                    "LayerId": "7cdaa765-3b1c-48c2-af03-2c8198fb641a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "7cdaa765-3b1c-48c2-af03-2c8198fb641a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "954c8464-aa56-4d74-af4a-8b44dcf69405",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}