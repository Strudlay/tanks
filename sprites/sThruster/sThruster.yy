{
    "id": "db9b356a-2e21-431a-afc5-c28b893ea41a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sThruster",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 2,
    "bbox_right": 27,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76221829-6365-41a1-a744-e3a2f04a8f3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db9b356a-2e21-431a-afc5-c28b893ea41a",
            "compositeImage": {
                "id": "234be0a4-a832-4409-8388-3f943c5505c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76221829-6365-41a1-a744-e3a2f04a8f3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b446a16-0bc8-4d96-a3f0-ca2c0cecb698",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76221829-6365-41a1-a744-e3a2f04a8f3a",
                    "LayerId": "b40f8aba-b95d-4ed8-8aa0-b10f66d44769"
                }
            ]
        },
        {
            "id": "4abb03ad-a5ca-49ba-973d-afe13e9695e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db9b356a-2e21-431a-afc5-c28b893ea41a",
            "compositeImage": {
                "id": "d1179a49-c2e9-45c2-ad20-8ef4ce90ba44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4abb03ad-a5ca-49ba-973d-afe13e9695e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8df32705-c0b4-481f-9009-9521dbeb77fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4abb03ad-a5ca-49ba-973d-afe13e9695e9",
                    "LayerId": "b40f8aba-b95d-4ed8-8aa0-b10f66d44769"
                }
            ]
        },
        {
            "id": "b2916478-1974-4ec8-9619-303655690b50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db9b356a-2e21-431a-afc5-c28b893ea41a",
            "compositeImage": {
                "id": "6c75ed21-b2ac-4629-9dec-fa5ddd8cba52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2916478-1974-4ec8-9619-303655690b50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d5a831e-8b90-47aa-a404-7e0b6433371c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2916478-1974-4ec8-9619-303655690b50",
                    "LayerId": "b40f8aba-b95d-4ed8-8aa0-b10f66d44769"
                }
            ]
        },
        {
            "id": "33e894f0-2cbc-4bc6-9297-5d533cc1242c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db9b356a-2e21-431a-afc5-c28b893ea41a",
            "compositeImage": {
                "id": "1a3c027c-e0f0-4023-aed7-b82100f4c137",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33e894f0-2cbc-4bc6-9297-5d533cc1242c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "299ee854-c58f-4f43-9c24-3ce1c4a1ee70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33e894f0-2cbc-4bc6-9297-5d533cc1242c",
                    "LayerId": "b40f8aba-b95d-4ed8-8aa0-b10f66d44769"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b40f8aba-b95d-4ed8-8aa0-b10f66d44769",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db9b356a-2e21-431a-afc5-c28b893ea41a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 46,
    "yorig": 13
}