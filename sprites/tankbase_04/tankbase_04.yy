{
    "id": "dc407638-57ae-4509-ae5d-5fc6e7a5b2a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tankbase_04",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 85,
    "bbox_left": 8,
    "bbox_right": 89,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b41da023-2a1f-4231-ad63-d5ba7836d72b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc407638-57ae-4509-ae5d-5fc6e7a5b2a1",
            "compositeImage": {
                "id": "025f7931-1ae0-42ef-ac54-99754d60d7cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b41da023-2a1f-4231-ad63-d5ba7836d72b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87ce102a-8d0b-40e3-b5cd-0cbd56400d71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b41da023-2a1f-4231-ad63-d5ba7836d72b",
                    "LayerId": "376fef1a-2c80-4bf7-99be-b745ecf7d710"
                }
            ]
        },
        {
            "id": "29d88868-2e86-4427-9921-15362fc29c11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc407638-57ae-4509-ae5d-5fc6e7a5b2a1",
            "compositeImage": {
                "id": "07020d06-6bdf-4515-b69f-e1a00379356f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29d88868-2e86-4427-9921-15362fc29c11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66ba9a83-d6c4-4cae-a141-812bb2ebcb41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29d88868-2e86-4427-9921-15362fc29c11",
                    "LayerId": "376fef1a-2c80-4bf7-99be-b745ecf7d710"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "376fef1a-2c80-4bf7-99be-b745ecf7d710",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc407638-57ae-4509-ae5d-5fc6e7a5b2a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}