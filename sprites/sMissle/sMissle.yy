{
    "id": "1dbd79c8-66f7-4840-8db1-e457e6f3e76d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMissle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 1,
    "bbox_right": 28,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9dc08f44-0d53-4b03-a5c5-dc6d06d06ee4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dbd79c8-66f7-4840-8db1-e457e6f3e76d",
            "compositeImage": {
                "id": "5862ff47-7a21-41d0-b8fb-29d632825781",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dc08f44-0d53-4b03-a5c5-dc6d06d06ee4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "888bae2c-1d1e-4863-a3d6-38caa8d00784",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dc08f44-0d53-4b03-a5c5-dc6d06d06ee4",
                    "LayerId": "28104220-7cbd-4c81-a9e4-ade7f6745d79"
                }
            ]
        },
        {
            "id": "9307876a-e0b3-4d64-9677-c8e352fdfd0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dbd79c8-66f7-4840-8db1-e457e6f3e76d",
            "compositeImage": {
                "id": "030379ef-8079-4bed-846e-d0680ccbf607",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9307876a-e0b3-4d64-9677-c8e352fdfd0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd866977-4bdd-4a0e-8032-3e1170718f01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9307876a-e0b3-4d64-9677-c8e352fdfd0a",
                    "LayerId": "28104220-7cbd-4c81-a9e4-ade7f6745d79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "28104220-7cbd-4c81-a9e4-ade7f6745d79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1dbd79c8-66f7-4840-8db1-e457e6f3e76d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": -16,
    "yorig": 16
}