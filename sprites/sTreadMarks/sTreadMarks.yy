{
    "id": "bed81308-df4f-4fea-bd18-bfb7eb1e1480",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTreadMarks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cdacb3c8-e2a9-4b50-a496-ff1bd5d91993",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bed81308-df4f-4fea-bd18-bfb7eb1e1480",
            "compositeImage": {
                "id": "15af2cb6-7195-4de9-a051-846fdb30ee28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdacb3c8-e2a9-4b50-a496-ff1bd5d91993",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f06acb79-f130-4f26-a52e-009e476722ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdacb3c8-e2a9-4b50-a496-ff1bd5d91993",
                    "LayerId": "e47f85e0-0bb9-4667-af54-29e70552c51d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e47f85e0-0bb9-4667-af54-29e70552c51d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bed81308-df4f-4fea-bd18-bfb7eb1e1480",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}