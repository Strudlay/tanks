{
    "id": "1aa510f3-05f2-489e-a987-7e90b9fa00c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tankcannon_01a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 21,
    "bbox_right": 94,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fd339a7-26f9-49a1-a198-826f23fb293a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1aa510f3-05f2-489e-a987-7e90b9fa00c6",
            "compositeImage": {
                "id": "9c625e14-919c-43ff-9a1f-344d731a84c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fd339a7-26f9-49a1-a198-826f23fb293a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81ccc30f-484a-45d5-bfbd-57476d52124a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fd339a7-26f9-49a1-a198-826f23fb293a",
                    "LayerId": "f92e7e69-5443-441c-9e74-8d93c915c1e9"
                }
            ]
        },
        {
            "id": "ff39a9d1-e328-41c5-9997-6b9006e50340",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1aa510f3-05f2-489e-a987-7e90b9fa00c6",
            "compositeImage": {
                "id": "de7dd1cb-00bf-4be9-8c8c-461b072e0748",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff39a9d1-e328-41c5-9997-6b9006e50340",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3578aec-f3c3-4ad7-8998-48e6361604a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff39a9d1-e328-41c5-9997-6b9006e50340",
                    "LayerId": "f92e7e69-5443-441c-9e74-8d93c915c1e9"
                }
            ]
        }
    ],
    "gridX": 96,
    "gridY": 96,
    "height": 96,
    "layers": [
        {
            "id": "f92e7e69-5443-441c-9e74-8d93c915c1e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1aa510f3-05f2-489e-a987-7e90b9fa00c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 30,
    "yorig": 46
}