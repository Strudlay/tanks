{
    "id": "19afa4ae-6fa3-4a8e-b147-7c060f370c4c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tankcannon_02a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 14,
    "bbox_right": 81,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "480c6915-d761-4f8d-ac3b-5039219f3cce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19afa4ae-6fa3-4a8e-b147-7c060f370c4c",
            "compositeImage": {
                "id": "a5faf387-a26f-4890-8975-de4f98fa6d34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "480c6915-d761-4f8d-ac3b-5039219f3cce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d2cfe08-b3de-434a-9860-2c93305794a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "480c6915-d761-4f8d-ac3b-5039219f3cce",
                    "LayerId": "63287139-51c5-4ff1-93e8-94d6c940c000"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "63287139-51c5-4ff1-93e8-94d6c940c000",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19afa4ae-6fa3-4a8e-b147-7c060f370c4c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 34,
    "yorig": 48
}