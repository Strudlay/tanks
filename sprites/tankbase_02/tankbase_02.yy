{
    "id": "76717166-b658-4abb-9baf-701afb2b4323",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tankbase_02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 6,
    "bbox_right": 89,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e194bdca-9d99-4f7c-82fb-8d27cef2b030",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76717166-b658-4abb-9baf-701afb2b4323",
            "compositeImage": {
                "id": "3e2b90d2-1d4a-4829-8c7d-b208237aa37a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e194bdca-9d99-4f7c-82fb-8d27cef2b030",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3989b8d-b1a4-434a-81f9-ad4c9b1d513f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e194bdca-9d99-4f7c-82fb-8d27cef2b030",
                    "LayerId": "4afab51b-5963-4f59-b3b9-c65f2e001ca9"
                }
            ]
        },
        {
            "id": "7417d8f9-cfa9-4bca-a3da-b8782d372488",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76717166-b658-4abb-9baf-701afb2b4323",
            "compositeImage": {
                "id": "832b38b1-efae-4fc7-8fa9-182800d349da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7417d8f9-cfa9-4bca-a3da-b8782d372488",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76bc70e5-0fd0-4985-a98f-5ddd11c360f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7417d8f9-cfa9-4bca-a3da-b8782d372488",
                    "LayerId": "4afab51b-5963-4f59-b3b9-c65f2e001ca9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "4afab51b-5963-4f59-b3b9-c65f2e001ca9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76717166-b658-4abb-9baf-701afb2b4323",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}