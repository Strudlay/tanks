{
    "id": "53f228a2-65dd-4427-a17d-0ca5ad3cc2aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tankbase_05",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 85,
    "bbox_left": 6,
    "bbox_right": 91,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7824a323-ce7f-4d6b-a79c-a1283a098074",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53f228a2-65dd-4427-a17d-0ca5ad3cc2aa",
            "compositeImage": {
                "id": "c0b54e20-345d-4c61-8239-bd2cb08d4734",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7824a323-ce7f-4d6b-a79c-a1283a098074",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b8a5fef-5f49-4a94-a606-be0b74187f8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7824a323-ce7f-4d6b-a79c-a1283a098074",
                    "LayerId": "c7479358-44cc-4538-8e8d-e8570db11932"
                }
            ]
        },
        {
            "id": "084a95ce-aac5-47cb-add0-309f8abe3201",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53f228a2-65dd-4427-a17d-0ca5ad3cc2aa",
            "compositeImage": {
                "id": "bac59517-b9bd-46f0-b482-2cac5e0666a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "084a95ce-aac5-47cb-add0-309f8abe3201",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eb702fe-6dbe-4591-b68c-d66c046fa0de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "084a95ce-aac5-47cb-add0-309f8abe3201",
                    "LayerId": "c7479358-44cc-4538-8e8d-e8570db11932"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "c7479358-44cc-4538-8e8d-e8570db11932",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53f228a2-65dd-4427-a17d-0ca5ad3cc2aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}