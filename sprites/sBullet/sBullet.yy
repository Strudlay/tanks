{
    "id": "48f1137b-3e7b-43de-a8c3-919a592945ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 41,
    "bbox_right": 63,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "698c63ab-ddf9-4e9c-ab45-c9ac9a34bc07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f1137b-3e7b-43de-a8c3-919a592945ea",
            "compositeImage": {
                "id": "165b2415-e51f-439b-b691-9dc21059724f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "698c63ab-ddf9-4e9c-ab45-c9ac9a34bc07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0a51a7b-c3c0-47aa-84ae-f1b88698f8b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "698c63ab-ddf9-4e9c-ab45-c9ac9a34bc07",
                    "LayerId": "f7d42e11-2c12-435e-be08-4dcb9c128c48"
                }
            ]
        },
        {
            "id": "32978915-f3b3-481d-98d4-4ab243e22db5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f1137b-3e7b-43de-a8c3-919a592945ea",
            "compositeImage": {
                "id": "e59c38aa-b2ca-46ac-990f-e0c191c52921",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32978915-f3b3-481d-98d4-4ab243e22db5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a731839-16d5-4ad6-8a3a-e6be810d7c11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32978915-f3b3-481d-98d4-4ab243e22db5",
                    "LayerId": "f7d42e11-2c12-435e-be08-4dcb9c128c48"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 32,
    "height": 32,
    "layers": [
        {
            "id": "f7d42e11-2c12-435e-be08-4dcb9c128c48",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48f1137b-3e7b-43de-a8c3-919a592945ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 14
}