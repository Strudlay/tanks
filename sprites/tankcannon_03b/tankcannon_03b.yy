{
    "id": "8dc9784d-b6b6-42e6-bc98-e005cbc2cd4f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tankcannon_03b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 12,
    "bbox_right": 83,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07e27ad7-0430-4a6a-9cac-0a8b0697eee5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dc9784d-b6b6-42e6-bc98-e005cbc2cd4f",
            "compositeImage": {
                "id": "cea14d28-fe30-44d1-bcfe-e064a2e79de0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07e27ad7-0430-4a6a-9cac-0a8b0697eee5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "806e5a7e-04f0-4061-97b0-76143acebbc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07e27ad7-0430-4a6a-9cac-0a8b0697eee5",
                    "LayerId": "6c10cbe5-0cba-4970-baad-eb8ff130c734"
                }
            ]
        },
        {
            "id": "03df15e1-07ad-403e-bec8-4cd2ba39f880",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dc9784d-b6b6-42e6-bc98-e005cbc2cd4f",
            "compositeImage": {
                "id": "d583dff1-9861-46ff-a157-5a80961052c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03df15e1-07ad-403e-bec8-4cd2ba39f880",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15137947-f3e3-4b1e-a8b6-3b167c041e4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03df15e1-07ad-403e-bec8-4cd2ba39f880",
                    "LayerId": "6c10cbe5-0cba-4970-baad-eb8ff130c734"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "6c10cbe5-0cba-4970-baad-eb8ff130c734",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8dc9784d-b6b6-42e6-bc98-e005cbc2cd4f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 34,
    "yorig": 48
}