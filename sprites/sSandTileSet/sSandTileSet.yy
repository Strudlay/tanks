{
    "id": "8106da7b-a25d-495e-8f91-736eb677e551",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSandTileSet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2111,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbbbfc6f-d057-48ef-b749-d5b660a2c8f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8106da7b-a25d-495e-8f91-736eb677e551",
            "compositeImage": {
                "id": "e8f12f12-a748-4535-b524-a5c1e728b358",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbbbfc6f-d057-48ef-b749-d5b660a2c8f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e79e9530-f3b9-46fe-b350-38f2182f4e7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbbbfc6f-d057-48ef-b749-d5b660a2c8f1",
                    "LayerId": "e3501de8-dab8-4db6-9968-b7522039386e"
                }
            ]
        }
    ],
    "gridX": 96,
    "gridY": 96,
    "height": 2112,
    "layers": [
        {
            "id": "e3501de8-dab8-4db6-9968-b7522039386e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8106da7b-a25d-495e-8f91-736eb677e551",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}