{
    "id": "b4f9be83-f307-4b23-9fd1-6e49939c2b4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSolid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b300fd05-5595-4289-a03b-ae8f563e03bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4f9be83-f307-4b23-9fd1-6e49939c2b4d",
            "compositeImage": {
                "id": "3d9b5bcf-29c9-4fb1-84d7-93f0717b98b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b300fd05-5595-4289-a03b-ae8f563e03bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67ed8a12-ed82-4a94-a181-a8038093aec6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b300fd05-5595-4289-a03b-ae8f563e03bc",
                    "LayerId": "f50edcc2-530e-4b8e-adf2-7ccedf2a41c4"
                }
            ]
        }
    ],
    "gridX": 96,
    "gridY": 96,
    "height": 96,
    "layers": [
        {
            "id": "f50edcc2-530e-4b8e-adf2-7ccedf2a41c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4f9be83-f307-4b23-9fd1-6e49939c2b4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}