{
    "id": "fff6c0f1-1bda-4e0f-850c-0d0251df820c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tankbase_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 14,
    "bbox_right": 81,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3cb4163b-4025-4a3f-8a6e-c47ba0712a75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fff6c0f1-1bda-4e0f-850c-0d0251df820c",
            "compositeImage": {
                "id": "ff79c500-4650-4b0b-b4bf-127254c98e77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cb4163b-4025-4a3f-8a6e-c47ba0712a75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a33400d-46fb-40b8-9d8a-3d303f1fa313",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cb4163b-4025-4a3f-8a6e-c47ba0712a75",
                    "LayerId": "5971ec9e-a905-425b-b583-f3e32e86e602"
                }
            ]
        },
        {
            "id": "028bd33a-62bc-48c0-bd7b-a3946d756449",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fff6c0f1-1bda-4e0f-850c-0d0251df820c",
            "compositeImage": {
                "id": "22a049db-9737-4213-a782-0c24a997d1b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "028bd33a-62bc-48c0-bd7b-a3946d756449",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "101deb6b-57f6-46c9-b993-e4d4b7be4f6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "028bd33a-62bc-48c0-bd7b-a3946d756449",
                    "LayerId": "5971ec9e-a905-425b-b583-f3e32e86e602"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "5971ec9e-a905-425b-b583-f3e32e86e602",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fff6c0f1-1bda-4e0f-850c-0d0251df820c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 38,
    "yorig": 46
}