{
    "id": "c44f1376-e553-4a05-9937-e01dd01634e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTileCollisions",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 96,
    "bbox_right": 287,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8501631a-315c-4596-9f7c-1e0be376962d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c44f1376-e553-4a05-9937-e01dd01634e7",
            "compositeImage": {
                "id": "471eaa57-931a-4f24-bf46-2593a8aff236",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8501631a-315c-4596-9f7c-1e0be376962d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01a02ca2-3fad-4165-81ab-e15827107d44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8501631a-315c-4596-9f7c-1e0be376962d",
                    "LayerId": "47b8a6c2-663e-4730-8ff8-7f715c759cf0"
                }
            ]
        }
    ],
    "gridX": 96,
    "gridY": 96,
    "height": 96,
    "layers": [
        {
            "id": "47b8a6c2-663e-4730-8ff8-7f715c759cf0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c44f1376-e553-4a05-9937-e01dd01634e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 288,
    "xorig": 0,
    "yorig": 0
}