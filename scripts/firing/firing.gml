// Player firing

firingDelay --;
// If player shoots and firing delay is less than 0
if((primaryShot) && (firingDelay < 0)) {
	// Create bullet
	var bullet = instance_create_layer(x, y, "Bullets", oBullet);
	// Using bullet
	with(bullet) {
		// Set image angle, direction, and speed
		image_angle = oPlayerTurret.image_angle;
		direction = image_angle;
		speed = 8;
	}
	// Reset firing delay
	firingDelay = 15;
}