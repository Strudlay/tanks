/// Collisions

/// ----- WORKING SOLUTION WITH OBJECTS ----- ///

// Horizontal Collision
if(place_meeting(x + hsp, y, oSolid)){
	while(!place_meeting(x + sign(hsp), y, oSolid)){
		x+= sign(hsp);	
	}
	hsp = 0;
}
// Apply movement
x += hsp;
// Vertical Collision
if(place_meeting(x, y + vsp, oSolid)){
	while(!place_meeting(x, y + sign(vsp), oSolid)){
		y+= sign(vsp);	
	}
	vsp = 0;
}
// Apply movement
y += vsp;