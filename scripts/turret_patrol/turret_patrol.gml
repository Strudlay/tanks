/// Turret Patrol
//image_angle = clamp(oEnemy1.direction, maxRange, minRange);

// Turret moves back and forth within range
image_angle += turretSpeed;
if (image_angle == minRange) {
	turretSpeed = -turretSpeed;
}
if (image_angle == maxRange) {
	turretSpeed = abs(turretSpeed);
}