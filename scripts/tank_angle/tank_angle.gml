/// @desc tank_angle(tank_sprite, team);
/// @arg0 tank_sprite
/// @arg1 team

// Set argument
var tank_sprite = argument0;
var team = argument1;
// Draw sprite and adjust angle
draw_sprite_ext(tank_sprite, team, x, y, 1, 1, direction, c_white, 1);