// Enemy Idle state

// Set gas
gas = 0;
// Set speeds
hsp = 0;
vsp = 0;
// Apply movement and collision
enemy_collision();
// Thruster
thruster(x - 8, y - 2);
// Checks for player
if (within_range(fieldOfView)) {
	state = enemy1_states.CHASE;
	turret.state = enemyTurret1_states.CHASE;
}