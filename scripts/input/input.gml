// Player controls
left = keyboard_check(ord("A"));
right = keyboard_check(ord("D"));
down = keyboard_check(ord("S"));
up = keyboard_check(ord("W"));
primaryShot = mouse_check_button(mb_left);
secondaryShot = mouse_check_button(mb_right);