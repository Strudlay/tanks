/// Collisions

// Horizontal and Vertical speeds
hsp = lengthdir_x(gas, direction);	
vsp = lengthdir_y(gas, direction);

/// ----- WORKING SOLUTION WITH OBJECTS ----- ///

// Horizontal Collision
if(place_meeting(x + hsp, y, oSolid)){
	while(!place_meeting(x + sign(hsp), y, oSolid)){
		x+= sign(hsp);	
	}
	hsp = 0;
}
// Apply movement
x += hsp;
// Vertical Collision
if(place_meeting(x, y + vsp, oSolid)){
	while(!place_meeting(x, y + sign(vsp), oSolid)){
		y+= sign(vsp);	
	}
	vsp = 0;
}
// Apply movement
y += vsp;

x=clamp(x,0 + (sprite_width/2),room_width-(sprite_width/2));
y=clamp(y,0 + (sprite_height/2),room_height-(sprite_height/2));

/// ----- WORKING SOLUTION WITH TILES ----- ///
/*
/// HORIZONTAL COLLISION 
// Determine which side to test
var side;
if hsp > 0 side = bbox_right else side = bbox_left;
// Check top and bottom
var t1 = tilemap_get_at_pixel(global.map, side + hsp, bbox_top);
var t2 = tilemap_get_at_pixel(global.map, side + hsp, bbox_bottom);
// Check if Solid
if ((t1 != VOID) || (t2 != VOID)){
	// Collision found
	if (hsp > 0) x = x - (x mod global.tile_size) + (global.tile_size - 1 - (side - x));
	else  x = x - (x mod global.tile_size) - (side - x);
	hsp = 0;
}
x += hsp;
/// VERTICAL COLLISION
// Determine which side to test
var side;
if vsp > 0 side = bbox_bottom else side = bbox_top;
// Check left and right
var t1 = tilemap_get_at_pixel(global.map, bbox_left, vsp + side);
var t2 = tilemap_get_at_pixel(global.map, bbox_right, vsp + side);
// Check if solid or void
if ((t1 != VOID) || (t2 != VOID)){
	// Collision found
	if (vsp > 0) y = y - (y mod global.tile_size) + (global.tile_size - 1 - (side - y));
	else  y = y - (y mod global.tile_size) - (side - y);
	vsp = 0;
}
y += vsp;
*/