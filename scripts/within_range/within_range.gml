/// @desc within_range(field_of_view);
/// @arg0 field_of_view
/// Changes speed and scaling of thruster when moving


// Set range for argument
var field_of_view = argument0;
var this_tank = self;
// Check for playe
with(field_of_view) {
	// Calculate distance to player
	var distance = point_distance(x, y, oPlayerTank.x, oPlayerTank.y)
	// If player detected
	if(place_meeting(x, y, oTankParent)){
		// If close come to halt
		if(distance < 150) {
			this_tank.gas = 0;	
		}
		// Otherwise keep going
		else {
			this_tank.gas = this_tank.spd;
		}
		// Change to spotted
		image_index = 1;
		return true;	
	}
	// If player not detected
	else if (!place_meeting(x, y, oTankParent)){
		image_index = 0;
		return false;
	}
}
/*
// Calculate distance to player
var distance = point_distance(x, y, oPlayerTank.x, oPlayerTank.y)
// If within distance change state
if(distance < sight_range){
	// If close come to halt
	if(distance < 150) {
		gas = 0;	
	}
	// Otherwise keep going
	else {
		gas = spd;
	}
	oFieldOfSight.image_index = 1;	
	return true;
}
// If player leaves sight
else if(distance > sight_range) {
	oFieldOfSight.image_index = 0;
	return false;
}