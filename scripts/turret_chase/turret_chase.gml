/// Turret chase state

// Calculate distance to player
var playerDir = point_direction(x, y, oTankParent.x, oTankParent.y);
var angleDif = angle_difference(image_angle, playerDir);
// Set direction
image_angle -= min(abs(angleDif), 1.5) * sign(angleDif);