// Enemy Chase state

// Set random stagger
var randomX = random_range(10, -10);
var randomY = random_range(10, -10);
// Set player direction
var playerDir = point_direction(x, y, oTankParent.x, oTankParent.y);
// Calculate angle difference
var angleDif = angle_difference(direction, playerDir);
// Set direction
direction -= min(abs(angleDif), 1) * sign(angleDif);
// Horizontal and Vertical speeds
hsp = lengthdir_x(gas, playerDir);	
vsp = lengthdir_y(gas, playerDir);
// Apply movement and collision
enemy_collision();
// Thruster
thruster(thrustX , thrustY);
// Checks for player
if (!within_range(fieldOfView)) {
	state = enemy1_states.IDLE;
	turret.state = enemyTurret1_states.PATROL;
	// Sets new position for turret view
	with(turret){
		maxRange = image_angle - 45;
		minRange = image_angle + 45;
		image_angle = (maxRange + minRange) / 2
	}
}
// Adjusts position
x+=hsp;
y+=vsp;