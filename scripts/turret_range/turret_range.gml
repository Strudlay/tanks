/// @desc turret_range(min_max_angle, rotation_speed);
/// Limits turret range
/// @arg min_max_angle
/// @arg rotation_speed

// Variables
var min_max_angle = argument0;
var rotation_speed = argument1;

// Angle to mouse
var pointDir = point_direction(x, y, mouse_x, mouse_y);
// Player tank angle
var baseAngle = oTankParent.direction;
// Sets max and mins based on the angle of the player tank
var delta = max(-min_max_angle, min(min_max_angle, angle_difference(pointDir, baseAngle)));
// Sets base difference
var baseDiff = baseAngle + delta;
// Recalibrates the image angle
image_angle = (image_angle + median(-rotation_speed, rotation_speed, angle_difference(baseDiff, image_angle)));