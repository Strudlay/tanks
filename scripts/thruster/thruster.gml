/// @desc thruster(thruster_x, thruster_y);
/// @arg thruster_x
/// @arg thruster_y
/// Changes speed and scaling of thruster when moving

// Set thruster coordinates
var thruster_x = argument0;
var thruster_y = argument1;
// Sets position and angle
thrust.x = thruster_x;
thrust.y = thruster_y;
thrust.image_angle = self.direction;
// Sets speed to 15 and scales up thruster
if(gas > 0){
	thrust.image_speed = 15;	
	thrust.image_yscale = 1.3;
	thrust.image_xscale = 1.3;
}
// Sets default speed to 0.5 and resets scaling
else {
	thrust.image_speed = 0.5;	
	thrust.image_yscale = 1;
	thrust.image_xscale = 1;
}