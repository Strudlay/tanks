/// @desc]
// Update the thrust coordinates
thrustX = x - 2;
thrustY = y - 2;
// Inherit the parent event
event_inherited();
// Clamp turret to tank
turret.x = clamp(x, self.x, self.x);
turret.y = clamp(y, self.y, self.y);
//turret.image_angle = direction;
//radius.x = x;
//radius.y = y;
fieldOfView.x = x;
fieldOfView.y = y;
fieldOfView.image_angle = turret.image_angle;