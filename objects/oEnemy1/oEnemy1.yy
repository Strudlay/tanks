{
    "id": "7e334406-c07b-44bb-8d5f-8554f664aea3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemy1",
    "eventList": [
        {
            "id": "4db2620f-8e63-40e3-bbba-0a2ddd4fb627",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7e334406-c07b-44bb-8d5f-8554f664aea3"
        },
        {
            "id": "cc5a999d-1373-4f8e-9d23-4842e516ee8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7e334406-c07b-44bb-8d5f-8554f664aea3"
        },
        {
            "id": "7cd4afcd-0822-4dc8-8000-70da58a32803",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7e334406-c07b-44bb-8d5f-8554f664aea3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "10aaf475-c685-4aaf-b901-04f10315ee85",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "954c8464-aa56-4d74-af4a-8b44dcf69405",
    "visible": true
}