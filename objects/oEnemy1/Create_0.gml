/// @desc

// Inherit the parent event
event_inherited();
// Set image index and speed
image_index = 1;
image_speed = 0;
// Set speed
spd = .5;
// Create Thruster
thrust = instance_create_layer(x, y, "Enemy", oThruster);
// Create turret
turret = instance_create_layer(x, y, "Turret", oEnemyTurret1);
// Create field of sight for testing
fieldOfView = instance_create_layer(x, y, "Enemy", oFieldOfView);
// States for tank
enum enemy1_states {
	IDLE,
	PATROL,
	ATTACK, 
	CHASE,
	RETREAT
}
// Default state
state = enemy1_states.IDLE;
// States array
states_array[enemy1_states.IDLE] = enemy_idle;
states_array[enemy1_states.PATROL] = enemy_patrol;
states_array[enemy1_states.ATTACK] = enemy_attack;
states_array[enemy1_states.CHASE] = enemy_chase;
//states_array[enemy1_states.RETREAT] = enemy_retreat;