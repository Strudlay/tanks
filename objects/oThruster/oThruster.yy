{
    "id": "e6ba47e5-2f10-456f-8099-5925834aaa5e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oThruster",
    "eventList": [
        {
            "id": "08ce7ae5-240f-4b32-9bb5-49a66d697cac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e6ba47e5-2f10-456f-8099-5925834aaa5e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "db9b356a-2e21-431a-afc5-c28b893ea41a",
    "visible": true
}