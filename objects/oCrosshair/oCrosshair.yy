{
    "id": "fb79ac24-be81-49f3-9877-b4486088faf6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCrosshair",
    "eventList": [
        {
            "id": "d1ed0c1e-b993-44ce-b317-7d094dce0aef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fb79ac24-be81-49f3-9877-b4486088faf6"
        },
        {
            "id": "7c8125c1-0c39-419f-a5d7-5eb349814ac6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fb79ac24-be81-49f3-9877-b4486088faf6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "48d533bb-3624-427a-9d83-6bd8c1132031",
    "visible": true
}