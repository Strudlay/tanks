{
    "id": "6aa1ae47-39a6-40a6-bf00-849a0a9d1e0e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRadius",
    "eventList": [
        {
            "id": "71860732-19af-4a1f-8e27-f7db61203b86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6aa1ae47-39a6-40a6-bf00-849a0a9d1e0e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2ab4881d-99bc-48f1-9c68-8c38d66363e3",
    "visible": true
}