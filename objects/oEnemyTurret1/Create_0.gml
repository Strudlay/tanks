/// @desc

// Inherit the parent event
event_inherited();

// States for turret
enum enemyTurret1_states {
	PATROL,
	CHASE,
	ATTACK
}
// Default state
state = enemyTurret1_states.PATROL;
// States array
states_array[enemyTurret1_states.PATROL] = turret_patrol;
states_array[enemyTurret1_states.CHASE] = turret_chase;
states_array[enemyTurret1_states.ATTACK] = turret_attack;