{
    "id": "1358b32a-0141-47d2-a0be-8632944bf295",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemyTurret1",
    "eventList": [
        {
            "id": "3d181547-700a-48c1-81e6-47efadbc0fc9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1358b32a-0141-47d2-a0be-8632944bf295"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d9de3461-66e8-4dbe-a41d-5ff39107b643",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8dc9784d-b6b6-42e6-bc98-e005cbc2cd4f",
    "visible": true
}