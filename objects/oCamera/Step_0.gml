/// @desc

// Update cam destination
if (instance_exists(follow)) {
	xTo = follow.x;
	yTo = follow.y;
}
// Update cam position
x += (xTo - x) / 25;
y += (yTo - y) / 25;
// Update cam view
camera_set_view_pos(cam, x - view_w_half, y - view_h_half);