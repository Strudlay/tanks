/// @desc

// Sprite
tankSprite = sprite_index;
// Speeds
hsp = 0;
vsp = 0;
gas = 0;
spd = 0;
// Rotation
rotationSpeed = 2;
tankAngle = 0;
// Firing
firingDelay = 0;
// Health
hp = 5;
flash = 0;
// Thruster
thrustX = 0;
thrustY = 0;
// Stagger
staggerDelay = room_speed * 4;
// Create radius for testing
//radius = instance_create_layer(x, y, "Enemy", oRadius);