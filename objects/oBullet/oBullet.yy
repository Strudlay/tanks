{
    "id": "50f9028f-7628-47c7-ac3d-ce383e610db0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBullet",
    "eventList": [
        {
            "id": "70a6f985-0e92-4902-a1c5-3e1f4f00eb3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "50f9028f-7628-47c7-ac3d-ce383e610db0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "48f1137b-3e7b-43de-a8c3-919a592945ea",
    "visible": true
}