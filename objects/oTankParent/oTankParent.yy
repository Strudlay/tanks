{
    "id": "a23e0ac0-7183-436d-9fe1-635dfa848977",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTankParent",
    "eventList": [
        {
            "id": "c80a4735-3eef-41af-bee8-c3cff2db68ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a23e0ac0-7183-436d-9fe1-635dfa848977"
        },
        {
            "id": "4fb9737a-f354-4ad0-a54c-52acd08e2636",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a23e0ac0-7183-436d-9fe1-635dfa848977"
        },
        {
            "id": "88b64895-aa19-4c66-945e-a7486a4fa5bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a23e0ac0-7183-436d-9fe1-635dfa848977"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "71e188b7-c836-474a-be49-e2f5209c49fd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "78dfec4d-b162-4567-a0cd-9bd52aa6e0a5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 0
        },
        {
            "id": "545146fa-a602-461c-afaf-98ebf4ff32ba",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 48
        },
        {
            "id": "4be62fce-d8fe-46e0-8d9b-e9260e4fd7d1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 48
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}