/// @desc

// Controls
left = 0;
right = 0;
down = 0;
up = 0;
primaryShot = 0;
secondaryShot = 0;
// Speeds
hsp = 0;
vsp = 0;
gas = 0;
spd = 4;
// Rotation
rotationSpeed = 2;
tankAngle = 0;
// Firing
firingDelay = 0;
// Set chosen tank
tankSprite = tankbase_01;
sprite_index = tankSprite;
// Create chosen turret
playerTurret = tankcannon_01a;
turret = instance_create_layer(x, y, "Turret", oPlayerTurret);
// Create thruster
thrust = instance_create_layer(x, y, "Player", oThruster);
// Create crosshair
instance_create_layer(x, y, "Turret", oCrosshair);