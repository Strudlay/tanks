{
    "id": "062d210a-b045-4c39-8f5b-2f59997a50f5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFieldOfView",
    "eventList": [
        {
            "id": "f4d6baf5-78d7-44db-9cbe-b9e556f18877",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "062d210a-b045-4c39-8f5b-2f59997a50f5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "954af1b6-0a01-4880-8211-f36b7cb75197",
    "visible": true
}