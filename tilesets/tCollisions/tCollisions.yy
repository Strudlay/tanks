{
    "id": "7b643bbf-eda7-43c9-a697-b928724929f3",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tCollisions",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 2,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "c44f1376-e553-4a05-9937-e01dd01634e7",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 3,
    "tileheight": 96,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 96,
    "tilexoff": 0,
    "tileyoff": 0
}